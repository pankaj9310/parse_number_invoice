Rails.application.routes.draw do
  root 'user_story#index'
  post "user_story/upload"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
